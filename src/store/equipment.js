export default {
   namespaced: true,

   state: {
		fields: ['ФИО', 'Инв.номер', 'Компьютер', 'Монитор', 'Клавиатура', 'Мышь'],

		items: [
			{
				first_name: 'Лазарчук',
				inv_number: 1401401230012,
				pc: ['IBM','GeForce GTX 1060','ssd (500 GB)','HDD'],
				monitor: ['Samsung'],
				kbd: 'A4 tech',
				mouse: 'A4'
			},
			{
				first_name: 'Бакай',
				inv_number: 1211001230010,
				pc: ['IBM','GeForce GTX 1060','ssd (500 GB)','HDD','Lorem ipsum dolor sit amet consectetur adipisicing elit Nam corrupt consequatur'],
				monitor: ['Philips','LG'],
				kbd: 'A4 tech',
				mouse: 'Logitech'
			},
			{
				first_name: 'Лошкарёв',
				inv_number: 1891001230019,
				pc: ['IBM','GeForce GTX 1060','ssd (500 GB)','HDD'],
				monitor: ['Samsung'],
				kbd: 'A4 tech',
				mouse: 'A4'
			},
			{
				first_name: 'Филатов',
				inv_number: 1381001230016,
				pc: ['IBM','GeForce GTX 1060','ssd (500 GB)','HDD'],
				monitor: ['Philips'],
				kbd: 'A4 tech',
				mouse: 'A4'
			}
		],
		url: {
			doc: 'https://inventory-pc-optimus.herokuapp.com/api/spec'
		},
		accessories_labels: [{
			type: 'Тип оборудования',
			inv_number: 'Инвертарный номер'
		}],
		accessories_data: [
			{
				pc: 'IBM',
				inv_number: 1003344388474
			},
			{
				pc: 'IBM',
				inv_number: 1003364388109
			}
		]
	},
	mutations: {

	},
	getters: {
		getItem(state, current) {
			return state.items[current]
		}
	}
}