import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import equipment from './equipment'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		auth, equipment
	}
})