/* eslint-disable no-console */
import axios from 'axios';
import router from '../router'



// const AUTH_URL = 'https://inventory-pc-optimus.herokuapp.com/api/auth'
// axios.defaults.AUTH_URL = 'https://inventory-pc-optimus.herokuapp.com/api/auth';
const AUTH_URL = process.env.AUTH_URL

export default {
   namespaced: true,
   state: {
     token: null,
     error: '',
     gratters: ''
   },
   mutations: {
     SET_TOKEN(state, payload) {
       state.token = payload.token
       localStorage.setItem("token", payload.token)
       router.push('/')
     },
     SET_ERROR(state, payload) {
        state.error = payload
     },
     CLEAR_NOTIFICATIONS(state){
       state.gratters = ''
       state.error = ''
     },
     HANDLE_LOGINED(state) {
      state.gratters = 'Поздравляем, вы успешно зарегистрировались!..'
      setTimeout(() => router.push('/login'), 2500)
     }
   },
   actions: {
    register({commit}, payload) {
      axios.post(`${AUTH_URL}/register`, payload)
        .then(() =>  commit('HANDLE_LOGINED'))
        .catch(error => commit('SET_ERROR', error))
    },
    login({commit}, payload){
      axios.post(`${AUTH_URL}/login`, payload)
      .then(response => commit('SET_TOKEN', response.data))
      .catch(error => commit('SET_ERROR', error.response.data.message))
    },

    fetchData(){
      let token = localStorage.token
      const config = { headers: {'Authorization' : token}}
      axios.get('https://inventory-pc-optimus.herokuapp.com/api/employee', config)
         .then(response => console.log('response', response))
         .catch(e => console.info('Ошибка при получении данных:', e))
    }
  }

}
// let token = localStorage.token
// const config = { headers: {'Authorization' : `Bearer ${token}`} }