import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import BModal from 'bootstrap-vue/es/components/modal/modal'
import BModalDirective from 'bootstrap-vue/es/directives/modal/modal'
import VHeader from '@/components/Header'
import MainTable from '@/components/MainTable'
import ModalEdit from '@/components/ModalEdit'

Vue.use(Router)

Vue.component('v-header', VHeader)
Vue.component('main-table', MainTable)
Vue.component('modal-edit', ModalEdit)
Vue.component('b-modal', BModal)
Vue.directive('b-modal', BModalDirective)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '',
      name: 'home',
      component: Home,
      meta: {title: 'Главная'}
    },
    {
      path: '/accessories',
      name: 'accessories',
      meta: {title: 'Комплектующие'},
      component: () => import('./views/Accessories.vue')
    },
    {
      path: '/login',
      name: 'login',
      meta: {title: 'Вход'},
      component: () => import('./views/Login.vue')
    },
    {
      path: '/register',
      name: 'register',
      meta: {title: 'Регистрация'},
      component: () => import('./views/Register.vue')
    }
  ]
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})

export default  router