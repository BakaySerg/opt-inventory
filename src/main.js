import Vue from 'vue'
import Vuelidate from 'vuelidate'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/scss/bootstrap.scss'
import 'bootstrap-vue/src/index.scss'
import App from './App.vue'
import router from './router'
import store from './store'
// import VueResource from 'vue-resource';

Vue.use(BootstrapVue)
Vue.use(Vuelidate)
// Vue.use(VueResource);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')